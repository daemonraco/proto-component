# ProtoComponent

## TL;DR
This is a very minimal tool to split your HTML into pieces and inject them as if they were components.

## Motivation
Let's say you get tasked with making a HTML (and its stylesheet) as a scaffold for a new dashboard and it end up looking amazing with its main section, the surrounding layout, menus, navbars, etc., all the goodies.

Now you want to create another HTML that showcases the same but with a different main content. So you make a copy of the previous HTML and replace the main section.

Now someone points out that the menu should have two or three more items in it... so... do you make the same change in both HTMLs?... what if you already have 10 copies with different main contents?

The first and not bad at all solution is to migrate to Angular, VueJS, React or some other similar framework, but that's a lot of code, work and configuration for simple HTML scaffold that will most likely get chopped off into pieces and integrated into a framework.



So, at this point you want to be able to inject common of HTML pieces as if they were components, but you don't want the headache of configuring a big framework... well, here is where __ProtoComponent__ comes in.

## Installation with NPM
To install it use this command:
```
npm install proto-component
```
Then add this at the of your `<body>` tag.

```html
<script src="node_modules/proto-component/dist/proto-component.min.js"></script>
```

## Direct Use

If  you don't even use NPM, you can just simply download one of these links:

* [proto-component.js](https://gitlab.com/daemonraco/proto-component/-/raw/master/dist/proto-component.js)
* [proto-component.min.js](https://gitlab.com/daemonraco/proto-component/-/raw/master/dist/proto-component.min.js)

And then simply added it as:

```html
<script src="proto-component.min.js"></script>
```

## Example

### Supposed Structure
For this examples we're going to suppose this list of assets:

* /
  * _proto/
    * footer.html
    * menu.html
  * index.html

### Index Example
This is an example of a html file using __ProtoComponent__:

```php+HTML
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example</title>

    <link rel="stylesheet" href="https://bootswatch.com/4/pulse/bootstrap.min.css">
</head>

<body>
    <proto-component proto="menu"></proto-component>

    <div class="container pt-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Title</div>

                    <div class="card-body">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni sapiente rerum quo voluptatum sit
                        architecto, cumque corrupti. Veritatis, in voluptatum.
                    </div>
                </div>
            </div>
        </div>

        <proto-component proto="footer.html"></proto-component>
    </div>

    <script src="https://bootswatch.com/_vendor/jquery/dist/jquery.min.js"></script>
    <script src="https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="node_modules/proto-component/dist/proto-component.min.js"></script>
</body>

</html>
```

This example uses the great [Bootswatch](https://bootswatch.com), please visit their page (_Disclaimer_: We are not associated with Bootswatch).

### Other Pieces
__menu.html__
```html
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
        aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                    aria-expanded="false">Dropdown</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Separated link</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
```

__footer.html__
```html
<hr class="mt-5" />

<div class="row">
    <div class="col text-center">
        This is a footer!
    </div>
</div>
```

## Proto Configuration

If you don't like the name of the folder `_proto` you can simply change it doing this:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- . . . -->
    </head>
    <body>
        <proto-config>
            {
            "sources": "my-directory"
            }
        </proto-config>

        <!-- . . . -->

        <script src="node_modules/proto-component/dist/proto-component.min.js"></script>
    </body>
</html>
```

You could also use a file for you configuration doing something like this:

```html
<proto-config src="my-conf.json"></proto-config>
```

### Assets

`proto-config` also allows you to quickly list the list of CSS and JS files you want to add, for example, you can replace this:

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example</title>

    <link rel="stylesheet" href="https://bootswatch.com/4/pulse/bootstrap.min.css">
</head>

<body>
   <!-- ... -->

    <script src="https://bootswatch.com/_vendor/jquery/dist/jquery.min.js"></script>
    <script src="https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="node_modules/proto-component/dist/proto-component.min.js"></script>
</body>

</html>
```

With this:

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example</title>
</head>

<body>
    <proto-config>
        {
            "scripts": [
                "https://bootswatch.com/_vendor/jquery/dist/jquery.min.js",
                "https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.bundle.min.js"
            ],
            "styles": [
                "https://bootswatch.com/4/pulse/bootstrap.min.css"
            ]
        }
    </proto-config>

    <!-- ... -->

    <script src="node_modules/proto-component/dist/proto-component.min.js"></script>
</body>

</html>
```
