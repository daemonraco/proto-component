"use strict";
var ProtoComponent;
(function (ProtoComponent) {
    ProtoComponent.BOOTSTRAP_VERSION = '4.3.1';
    ProtoComponent.JQUERY_VERSION = '3.5.1';
    ProtoComponent.POPPER_VERSION = '1.14.7';
})(ProtoComponent || (ProtoComponent = {}));
var ProtoComponent;
(function (ProtoComponent) {
    class PCError extends window.Error {
        constructor(message) {
            super(message);
        }
    }
    ProtoComponent.PCError = PCError;
    class AssetError extends PCError {
        constructor(message, assetPath) {
            super(message);
            this.assetPath = undefined;
            this.assetPath = assetPath;
        }
    }
    ProtoComponent.AssetError = AssetError;
})(ProtoComponent || (ProtoComponent = {}));
var ProtoComponent;
(function (ProtoComponent) {
    class Help {
        constructor() {
        }
        static MissingAssetHint(asset) {
            let out = '';
            switch (asset) {
                case '/node_modules/jquery/dist/jquery.min.js':
                case 'node_modules/jquery/dist/jquery.min.js':
                    out = `have you installed jQuery?, try '<code>npm install jquery</code>'.`;
                    break;
                case '/node_modules/bootstrap/dist/js/bootstrap.min.js':
                case 'node_modules/bootstrap/dist/js/bootstrap.min.js':
                    out = `have you installed jQuery?, try '<code>npm install bootstrap</code>'.`;
                    break;
                case '/node_modules/bootstrap/dist/css/bootstrap.min.css':
                case 'node_modules/bootstrap/dist/css/bootstrap.min.css':
                    out = `have you installed jQuery?, try '<code>npm install bootstrap</code>'.`;
                    break;
            }
            return out;
        }
    }
    ProtoComponent.Help = Help;
})(ProtoComponent || (ProtoComponent = {}));
var ProtoComponent;
(function (ProtoComponent) {
    let MagicAssetTypes;
    (function (MagicAssetTypes) {
        MagicAssetTypes["Script"] = "script";
        MagicAssetTypes["Style"] = "style";
    })(MagicAssetTypes || (MagicAssetTypes = {}));
    class Tools {
        constructor() {
        }
        static AddConfigListener(listener) {
            Tools.ConfigListeners.push(listener);
        }
        static Configuration() {
            return Tools.Config;
        }
        static ErrorMessage(message) {
            return `<div style="border:solid 1px red;color:red;padding:5px;" class="proto-error">`
                + `<b class="title">ProtoComponent Error:</b>`
                + `<p class="message">${message}</p>`
                + `<div>`;
        }
        static HasPendingUpdates() {
            return Tools.PendingUpdates;
        }
        static Start() {
            if (!this.IsStarted) {
                this.IsStarted = true;
                setTimeout(() => {
                    Tools.InitialPendingUpdates = false;
                }, 0);
            }
        }
        static async UpdateConfig(conf) {
            Tools.PendingUpdates = true;
            await Tools.MergeConfiguration(conf);
            Tools.PendingUpdates = false;
            await Tools.UpdateConfigListener();
        }
        static async UpdateConfigByPath(confPath) {
            Tools.PendingUpdates = true;
            try {
                const confResponse = await fetch(confPath);
                if (confResponse.status === 200) {
                    await Tools.MergeConfiguration(JSON.parse(await confResponse.text()));
                    Tools.PendingUpdates = false;
                    await Tools.UpdateConfigListener();
                }
                else {
                    throw new Error(`Fetch ${confPath}: ${confResponse.status} ${confResponse.statusText}`);
                }
            }
            catch (err) {
                Tools.PendingUpdates = false;
                throw err;
            }
        }
        static async WaitPendingUpdates(waitInitial = true) {
            if (waitInitial) {
                while (Tools.InitialPendingUpdates) {
                    await new Promise((r) => setTimeout(r, 50));
                }
            }
            while (Tools.HasPendingUpdates()) {
                await new Promise((r) => setTimeout(r, 50));
            }
        }
        static ConvertMagicAssets(type, code) {
            const out = [];
            switch (type) {
                case MagicAssetTypes.Script:
                    switch (code) {
                        case '@nm/jquery':
                            out.push('/node_modules/jquery/dist/jquery.min.js');
                            break;
                        case '@nm/bootstrap':
                            out.push('/node_modules/jquery/dist/jquery.min.js');
                            out.push('/node_modules/bootstrap/dist/js/bootstrap.min.js');
                            break;
                        case '@jquery':
                            out.push(`https://code.jquery.com/jquery-${ProtoComponent.JQUERY_VERSION}.min.js`);
                            console.log(`DEBUG: pasa por aca`);
                            break;
                        case '@popper.js':
                            out.push(`https://cdnjs.cloudflare.com/ajax/libs/popper.js/${ProtoComponent.POPPER_VERSION}/umd/popper.min.js`);
                            break;
                        case '@bootstrap':
                            out.push(`https://code.jquery.com/jquery-${ProtoComponent.JQUERY_VERSION}.min.js`);
                            out.push(`https://stackpath.bootstrapcdn.com/bootstrap/${ProtoComponent.BOOTSTRAP_VERSION}/js/bootstrap.min.js`);
                            break;
                        default:
                            out.push(code);
                            break;
                    }
                    break;
                case MagicAssetTypes.Style:
                    switch (code) {
                        case '@nm/bootstrap':
                            out.push('/node_modules/bootstrap/dist/css/bootstrap.min.css');
                            break;
                        case '@bootstrap':
                            out.push(`https://stackpath.bootstrapcdn.com/bootstrap/${ProtoComponent.BOOTSTRAP_VERSION}/css/bootstrap.min.css`);
                            break;
                        default:
                            out.push(code);
                            break;
                    }
                    break;
                default:
                    throw new Error(`Unknown magic asset type '${type}'`);
            }
            return out;
        }
        static async ImportAssets() {
            const header = document.querySelector('head');
            if (header) {
                for (const url of Tools.Configuration().styles || []) {
                    if (!Tools.ImportedStyles.includes(url)) {
                        Tools.ImportedStyles.push(url);
                        const elem = document.createElement('link');
                        elem.rel = 'stylesheet';
                        elem.href = url;
                        elem.type = 'text/css';
                        header.appendChild(elem);
                        await new Promise((resolve, reject) => {
                            elem.addEventListener('load', resolve);
                            elem.addEventListener('error', () => reject(new ProtoComponent.AssetError(`Unable to load '${url}'.`, url)));
                        });
                    }
                }
            }
            const body = document.querySelector('body');
            if (body) {
                for (const url of Tools.Configuration().scripts || []) {
                    if (!Tools.ImportedScripts.includes(url)) {
                        Tools.ImportedScripts.push(url);
                        const elem = document.createElement('script');
                        elem.src = url;
                        elem.type = 'text/javascript';
                        body.appendChild(elem);
                        await new Promise((resolve, reject) => {
                            elem.addEventListener('load', resolve);
                            elem.addEventListener('error', () => reject(new ProtoComponent.AssetError(`Unable to load '${url}'.`, url)));
                        });
                    }
                }
            }
        }
        static async MergeConfiguration(conf) {
            if (typeof conf.sources === 'string') {
                Tools.Config.sources = conf.sources;
            }
            if (Array.isArray(conf.scripts)) {
                Tools.Config.scripts = Tools.Config.scripts || [];
                for (const asset of conf.scripts) {
                    Tools.Config.scripts = [
                        ...Tools.Config.scripts,
                        ...Tools.ConvertMagicAssets(MagicAssetTypes.Script, asset),
                    ];
                }
            }
            if (Array.isArray(conf.styles)) {
                Tools.Config.styles = Tools.Config.styles || [];
                for (const asset of conf.styles) {
                    Tools.Config.styles = [
                        ...Tools.Config.styles,
                        ...Tools.ConvertMagicAssets(MagicAssetTypes.Style, asset),
                    ];
                }
            }
            await Tools.ImportAssets();
        }
        static async UpdateConfigListener() {
            for (const listener of Tools.ConfigListeners) {
                await listener();
            }
        }
    }
    Tools.Config = {
        scripts: [],
        sources: '_proto',
        styles: [],
    };
    Tools.ConfigListeners = [];
    Tools.ImportedScripts = [];
    Tools.ImportedStyles = [];
    Tools.InitialPendingUpdates = true;
    Tools.IsStarted = false;
    Tools.PendingUpdates = false;
    ProtoComponent.Tools = Tools;
})(ProtoComponent || (ProtoComponent = {}));
var ProtoComponent;
(function (ProtoComponent) {
    class Component extends HTMLElement {
        constructor() {
            super();
            ProtoComponent.Tools.AddConfigListener(() => {
                this.removeAttribute('analyzed');
                this.analyze();
            });
            this.analyze();
        }
        async analyze() {
            await ProtoComponent.Tools.WaitPendingUpdates();
            if (!this.getAttribute('analyzed')) {
                this.setAttribute('analyzed', 'true');
                let proto = this.getAttribute('proto');
                if (proto) {
                    try {
                        if (!proto.match(/\.html$/)) {
                            proto += '.html';
                        }
                        const protoPath = `${ProtoComponent.Tools.Configuration().sources}/${proto}`;
                        const protoResponse = await fetch(protoPath);
                        if (protoResponse.status === 200) {
                            this.innerHTML = await protoResponse.text();
                        }
                        else {
                            throw new Error(`Fetch ${protoPath}: ${protoResponse.status} ${protoResponse.statusText}`);
                        }
                    }
                    catch (err) {
                        this.innerHTML = ProtoComponent.Tools.ErrorMessage(`${err.message ? err.message : err}`);
                    }
                }
                else {
                    this.innerHTML = ProtoComponent.Tools.ErrorMessage(`The attribute 'proto' has not been set.`);
                }
            }
        }
    }
    ProtoComponent.Component = Component;
})(ProtoComponent || (ProtoComponent = {}));
var ProtoComponent;
(function (ProtoComponent) {
    class Config extends HTMLElement {
        constructor() {
            super();
            this.updateConfiguration();
        }
        async updateConfiguration() {
            await ProtoComponent.Tools.WaitPendingUpdates(false);
            if (!this.getAttribute('analyzed')) {
                const src = this.getAttribute('src');
                const debug = this.getAttribute('debug') !== null;
                try {
                    if (src) {
                        await ProtoComponent.Tools.UpdateConfigByPath(src);
                        if (!debug) {
                            this.remove();
                        }
                        else {
                            this.innerHTML = `<pre>`
                                + `${JSON.stringify(ProtoComponent.Tools.Configuration(), null, 4)}`
                                + `</pre>`;
                        }
                    }
                    else {
                        const conf = JSON.parse(this.innerHTML);
                        await ProtoComponent.Tools.UpdateConfig(conf);
                        if (!debug) {
                            this.remove();
                        }
                        else {
                            this.innerHTML = `<pre>`
                                + `${JSON.stringify(ProtoComponent.Tools.Configuration(), null, 4)}`
                                + `</pre>`;
                        }
                    }
                }
                catch (err) {
                    let message = `${err.message ? err.message : err}`;
                    if (this.innerHTML.trim()) {
                        message += `<pre>${this.innerHTML}</pre>`;
                    }
                    if (err instanceof ProtoComponent.AssetError) {
                        const hint = ProtoComponent.Help.MissingAssetHint(err.assetPath || '');
                        if (hint) {
                            message += `<hr/><p><b>Hint:</b> ${hint}</p>`;
                        }
                    }
                    this.innerHTML = ProtoComponent.Tools.ErrorMessage(message);
                }
            }
        }
    }
    ProtoComponent.Config = Config;
})(ProtoComponent || (ProtoComponent = {}));
(() => {
    customElements.define('proto-config', ProtoComponent.Config);
    customElements.define('proto-component', ProtoComponent.Component);
    ProtoComponent.Tools.Start();
})();
//# sourceMappingURL=proto-component.js.map