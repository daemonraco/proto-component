// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace ProtoComponent {
    export class PCError extends window.Error {
        //
        // Construction.
        constructor(message: string) {
            super(message);
        }
    }

    export class AssetError extends PCError {
        //
        // Properties.
        public assetPath: string | undefined = undefined;
        //
        // Construction.
        constructor(message: string, assetPath: string) {
            super(message);

            this.assetPath = assetPath;
        }
    }
}