// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace ProtoComponent {
    enum MagicAssetTypes {
        Script = 'script',
        Style = 'style',
    }

    interface IToolsConfig {
        scripts?: string[];
        sources?: string;
        styles?: string[];
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        [key: string]: any;
    }

    export class Tools {
        //
        // Class properties.
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        public static Config: IToolsConfig = {
            scripts: [],
            sources: '_proto',
            styles: [],
        };
        protected static ConfigListeners: (() => void)[] = [];
        protected static ImportedScripts: string[] = [];
        protected static ImportedStyles: string[] = [];
        protected static InitialPendingUpdates: boolean = true;
        protected static IsStarted: boolean = false;
        protected static PendingUpdates: boolean = false;
        //
        // Construction.
        protected constructor() {
            // This is a singleton.
        }
        //
        // Public class methods.
        public static AddConfigListener(listener: () => void): void {
            Tools.ConfigListeners.push(listener);
        }
        public static Configuration(): IToolsConfig {
            return Tools.Config;
        }
        public static ErrorMessage(message: string): string {
            return `<div style="border:solid 1px red;color:red;padding:5px;" class="proto-error">`
                + `<b class="title">ProtoComponent Error:</b>`
                + `<p class="message">${message}</p>`
                + `<div>`;
        }
        public static HasPendingUpdates(): boolean {
            return Tools.PendingUpdates;
        }
        public static Start(): void {
            if (!this.IsStarted) {
                this.IsStarted = true;

                setTimeout(() => {
                    Tools.InitialPendingUpdates = false;
                }, 0);
            }
        }
        public static async UpdateConfig(conf: IToolsConfig): Promise<void> {
            Tools.PendingUpdates = true;
            await Tools.MergeConfiguration(conf);
            Tools.PendingUpdates = false;

            await Tools.UpdateConfigListener();
        }
        public static async UpdateConfigByPath(confPath: string): Promise<void> {
            Tools.PendingUpdates = true;

            try {
                const confResponse: Response = await fetch(confPath);
                if (confResponse.status === 200) {
                    await Tools.MergeConfiguration(JSON.parse(await confResponse.text()));

                    Tools.PendingUpdates = false;
                    await Tools.UpdateConfigListener();
                } else {
                    throw new Error(`Fetch ${confPath}: ${confResponse.status} ${confResponse.statusText}`);
                }
            } catch (err) {
                Tools.PendingUpdates = false;
                throw err;
            }
        }
        public static async WaitPendingUpdates(waitInitial: boolean = true): Promise<void> {
            if (waitInitial) {
                while (Tools.InitialPendingUpdates) {
                    await new Promise((r: () => void) => setTimeout(r, 50));
                }
            }

            while (Tools.HasPendingUpdates()) {
                await new Promise((r: () => void) => setTimeout(r, 50));
            }
        }
        //
        // Protected class methods.
        protected static ConvertMagicAssets(type: MagicAssetTypes, code: string): string[] {
            const out: string[] = [];

            switch (type) {
                case MagicAssetTypes.Script:
                    switch (code) {
                        case '@nm/jquery':
                            out.push('/node_modules/jquery/dist/jquery.min.js');
                            break;
                        case '@nm/bootstrap':
                            out.push('/node_modules/jquery/dist/jquery.min.js');
                            out.push('/node_modules/bootstrap/dist/js/bootstrap.min.js');
                            break;
                        case '@jquery':
                            out.push(`https://code.jquery.com/jquery-${JQUERY_VERSION}.min.js`);
                            console.log(`DEBUG: pasa por aca`,);
                            break;
                        case '@popper.js':
                            out.push(`https://cdnjs.cloudflare.com/ajax/libs/popper.js/${POPPER_VERSION}/umd/popper.min.js`);
                            break;
                        case '@bootstrap':
                            out.push(`https://code.jquery.com/jquery-${JQUERY_VERSION}.min.js`);
                            out.push(`https://stackpath.bootstrapcdn.com/bootstrap/${BOOTSTRAP_VERSION}/js/bootstrap.min.js`);
                            break;
                        default:
                            out.push(code);
                            break;
                    }
                    break;
                case MagicAssetTypes.Style:
                    switch (code) {
                        case '@nm/bootstrap':
                            out.push('/node_modules/bootstrap/dist/css/bootstrap.min.css');
                            break;
                        case '@bootstrap':
                            out.push(`https://stackpath.bootstrapcdn.com/bootstrap/${BOOTSTRAP_VERSION}/css/bootstrap.min.css`);
                            break;
                        default:
                            out.push(code);
                            break;
                    }
                    break;
                default:
                    throw new Error(`Unknown magic asset type '${type}'`);
            }

            return out;
        }
        protected static async ImportAssets(): Promise<void> {
            const header: HTMLHeadElement | null = document.querySelector('head');
            if (header) {
                for (const url of Tools.Configuration().styles || []) {
                    if (!Tools.ImportedStyles.includes(url)) {
                        Tools.ImportedStyles.push(url);
                        const elem: HTMLLinkElement = document.createElement('link');
                        elem.rel = 'stylesheet';
                        elem.href = url;
                        elem.type = 'text/css';

                        header.appendChild(elem);

                        await new Promise((resolve: () => void, reject: (err: Error) => void) => {
                            elem.addEventListener('load', resolve);
                            elem.addEventListener('error', () =>
                                reject(new AssetError(`Unable to load '${url}'.`, url)));
                        });
                    }
                }
            }

            const body: HTMLBodyElement | null = document.querySelector('body');
            if (body) {
                for (const url of Tools.Configuration().scripts || []) {


                    if (!Tools.ImportedScripts.includes(url)) {
                        Tools.ImportedScripts.push(url);
                        const elem: HTMLScriptElement = document.createElement('script');
                        elem.src = url;
                        elem.type = 'text/javascript';

                        body.appendChild(elem);

                        await new Promise((resolve: () => void, reject: (err: Error) => void) => {
                            elem.addEventListener('load', resolve);
                            elem.addEventListener('error', () =>
                                reject(new AssetError(`Unable to load '${url}'.`, url)));
                        });
                    }
                }
            }
        }
        protected static async MergeConfiguration(conf: IToolsConfig): Promise<void> {
            if (typeof conf.sources === 'string') {
                Tools.Config.sources = conf.sources;
            }

            if (Array.isArray(conf.scripts)) {
                Tools.Config.scripts = Tools.Config.scripts || [];
                for (const asset of conf.scripts) {
                    Tools.Config.scripts = [
                        ...Tools.Config.scripts,
                        ...Tools.ConvertMagicAssets(MagicAssetTypes.Script, asset),
                    ];
                }
            }

            if (Array.isArray(conf.styles)) {
                Tools.Config.styles = Tools.Config.styles || [];
                for (const asset of conf.styles) {
                    Tools.Config.styles = [
                        ...Tools.Config.styles,
                        ...Tools.ConvertMagicAssets(MagicAssetTypes.Style, asset),
                    ];
                }
            }

            await Tools.ImportAssets();
        }
        protected static async UpdateConfigListener(): Promise<void> {
            for (const listener of Tools.ConfigListeners) {
                await listener();
            }
        }
    }
}