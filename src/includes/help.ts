// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace ProtoComponent {
    export class Help {
        //
        // Construction.
        protected constructor() {
            // This is a singleton.
        }
        //
        // Public class methods.
        public static MissingAssetHint(asset: string): string {
            let out: string = '';

            switch (asset) {
                case '/node_modules/jquery/dist/jquery.min.js':
                case 'node_modules/jquery/dist/jquery.min.js':
                    out = `have you installed jQuery?, try '<code>npm install jquery</code>'.`;
                    break;
                case '/node_modules/bootstrap/dist/js/bootstrap.min.js':
                case 'node_modules/bootstrap/dist/js/bootstrap.min.js':
                    out = `have you installed jQuery?, try '<code>npm install bootstrap</code>'.`;
                    break;
                case '/node_modules/bootstrap/dist/css/bootstrap.min.css':
                case 'node_modules/bootstrap/dist/css/bootstrap.min.css':
                    out = `have you installed jQuery?, try '<code>npm install bootstrap</code>'.`;
                    break;
            }

            return out;
        }
    }
}