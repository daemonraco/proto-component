// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace ProtoComponent {
    export const BOOTSTRAP_VERSION: string = '4.3.1';
    export const JQUERY_VERSION: string = '3.5.1';
    export const POPPER_VERSION: string = '1.14.7';
}
