// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace ProtoComponent {
    export class Config extends HTMLElement {
        //
        // Construction.
        constructor() {
            super();
            this.updateConfiguration();
        }
        //
        // Protected methods.
        protected async updateConfiguration(): Promise<void> {
            await Tools.WaitPendingUpdates(false);

            if (!this.getAttribute('analyzed')) {
                const src: string | null = this.getAttribute('src');
                const debug: boolean = this.getAttribute('debug') !== null;

                try {
                    if (src) {
                        await Tools.UpdateConfigByPath(src);
                        if (!debug) {
                            this.remove();
                        } else {
                            this.innerHTML = `<pre>`
                                + `${JSON.stringify(Tools.Configuration(), null, 4)}`
                                + `</pre>`;
                        }
                    } else {
                        const conf = JSON.parse(this.innerHTML);
                        await Tools.UpdateConfig(conf);

                        if (!debug) {
                            this.remove();
                        } else {
                            this.innerHTML = `<pre>`
                                + `${JSON.stringify(Tools.Configuration(), null, 4)}`
                                + `</pre>`;
                        }
                    }
                } catch (err) {
                    let message: string = `${err.message ? err.message : err}`;

                    if (this.innerHTML.trim()) {
                        message += `<pre>${this.innerHTML}</pre>`;
                    }

                    if (err instanceof AssetError) {
                        const hint: string = Help.MissingAssetHint(err.assetPath || '');
                        if (hint) {
                            message += `<hr/><p><b>Hint:</b> ${hint}</p>`;
                        }
                    }

                    this.innerHTML = Tools.ErrorMessage(message);
                }
            }
        }
    }
}
