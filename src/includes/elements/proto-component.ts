// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace ProtoComponent {
    export class Component extends HTMLElement {
        //
        // Construction.
        constructor() {
            super();

            Tools.AddConfigListener(() => {
                this.removeAttribute('analyzed');
                this.analyze();
            });
            this.analyze();
        }
        //
        // Public methods.

        //
        // Protected methods.
        protected async analyze(): Promise<void> {
            await Tools.WaitPendingUpdates();

            if (!this.getAttribute('analyzed')) {
                this.setAttribute('analyzed', 'true');

                let proto: string | null = this.getAttribute('proto');
                if (proto) {
                    try {
                        if (!proto.match(/\.html$/)) {
                            proto += '.html';
                        }

                        const protoPath: string = `${Tools.Configuration().sources}/${proto}`;
                        const protoResponse: Response = await fetch(protoPath);
                        if (protoResponse.status === 200) {
                            this.innerHTML = await protoResponse.text();
                        } else {
                            throw new Error(`Fetch ${protoPath}: ${protoResponse.status} ${protoResponse.statusText}`);
                        }
                    } catch (err) {
                        this.innerHTML = Tools.ErrorMessage(`${err.message ? err.message : err}`);
                    }
                } else {
                    this.innerHTML = Tools.ErrorMessage(`The attribute 'proto' has not been set.`);
                }
            }
        }
    }
}