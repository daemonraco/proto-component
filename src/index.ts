/// <reference path="./includes/conf.ts" />
/// <reference path="./includes/error.ts" />
/// <reference path="./includes/help.ts" />
/// <reference path="./includes/tools.ts" />
/// <reference path="./includes/elements/proto-component.ts" />
/// <reference path="./includes/elements/proto-config.ts" />

(() => {
    customElements.define('proto-config', ProtoComponent.Config);
    customElements.define('proto-component', ProtoComponent.Component);

    ProtoComponent.Tools.Start();
})();
